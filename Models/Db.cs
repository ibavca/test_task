using System.Collections.Generic;
using System.Linq;
using t1.bavc.ru.Models; 
using System;

namespace t1.bavc.ru {       
    public class Db {
        List<User> Users = new List<User>();
        int page;
        public int pageCount {get;set;}
        public int allPage{
            get
            {                
                return Users.Count%pageCount!=0?Users.Count/pageCount+1:Users.Count/pageCount;
            }
        }
        public Db(){
            page = 1;
            pageCount = 1;
            User [] users = {
                new User {Id = 1, firstName = "Anton", lastName = "Belonogov"},
                new User {Id = 2, firstName = "Anna", lastName = "Belonogova"},
                new User {Id = 3, firstName = "Alena", lastName = "Belonogova"}
            };                        
            Users.AddRange(users);
        }
        public User GetById(int id) {
            return Users.Find(x=>x.Id == id);
        }
        public List<User> GetAll(int page){
            return Users.OrderBy(i=>i.Id).Skip((page-1)*pageCount).Take(pageCount).ToList();
        }
        public List<User> GetAll(){
            return Users;
        }
    }
}