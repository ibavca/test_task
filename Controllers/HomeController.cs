﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using t1.bavc.ru.Models;

namespace t1.bavc.ru.Controllers
{
    public class HomeController : Controller
    {
        private readonly Db _db;
        public HomeController (Db db){
            _db = db;
        }
        public IActionResult Index()
        {
            return View(new DbViewModel{Users= _db.GetAll(0), pageCount = _db.allPage});
            //return View(_db.GetAll(0));
        }

        public IActionResult PersonInfo()
        {
            return PartialView();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
