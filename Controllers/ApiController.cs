using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using t1.bavc.ru.Models;

namespace t1.bavc.ru.Controllers
{
    public class ApiController : Controller
    {
        private readonly Db _db;
        public ApiController(Db db){
            _db = db;
        }
        public IActionResult Index(){
            return Json(_db.GetAll());            
        }
       
        [HttpGet]
        public IActionResult Index(int page){
            var resultPage = _db.GetAll(page);
            return Json(resultPage);
        }
        [HttpGet]
        public int pageCount(){
            return _db.allPage;
        }
    }
}